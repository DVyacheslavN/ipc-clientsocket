/*
** Copyright (c) 2019 Domnin V.N.
** All rights reserved
**
**
*/
#ifndef CLIENTIFACE_H
#define CLIENTIFACE_H

#include <Package.h>

#include <functional>
#include <string>

class ClientIface
{

public:
    enum Status
    {
        Disconnected = 0,
        Connected,
        NotRespond,
    } typedef Status;

    static ClientIface *createClient(std::string address, ushort port);

    virtual void connect() = 0;
    virtual std::string ip() = 0;
    virtual Status sessionStatus() = 0;
    virtual bool sendPackage(const Package &package) = 0;

    virtual void bindPackageRecived(std::function<void (const Package &)> callBack_getPackage) = 0;
    virtual void bindSessionStatusChanged(std::function<void (Status)> handlerSessionStatusChanged) = 0;
    virtual void bindErrorRecived(std::function<void (std::string)>) = 0;
    virtual ~ClientIface() {}
};

#endif
