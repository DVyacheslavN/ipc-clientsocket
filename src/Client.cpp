/*
** Copyright (c) 2019 Domnin V.N.
** All rights reserved
**
**
*/

#include "Client.h"
#include <Package.h>

#include <thread>
#include <functional>

#ifdef CLIENT_DBUG
#include <debug.h>

#define debug() mDebug()
#define warn() mWarning()
#define inform() mMessage()
#else
struct Stub {
    template<class T>
    Stub& operator<<(const T&){
        return *this;
    }
    template<class T, class X>
    Stub& write(T, X) {
        return *this;
    }
};
#define debug() Stub()
#define warn() Stub()
#define inform() Stub()
#endif

using namespace boost::asio;

namespace
{
const std::vector<char> ClientPing = {'C','l','i','e','n','t','P','i','n','g'};
const std::vector<char> ServerReply = {'S','e','r','v','e','r','R','e','p','l','y'};

const uint PingErrorThreshold = 5;
}

#define DefaultBufferSize 1024

ClientIface* ClientIface::createClient(std::string address, ushort port)
{
    return new ClientPrivate(address, port);
}


ClientPrivate::ClientPrivate(std::string &address, ushort port)
    : m_sessionStatus(ClientIface::Disconnected)
    , m_ioService(new boost::asio::io_service)
    , m_session(SessionIface::CreateSession(m_ioService, std::make_shared<ip::tcp::socket>(*m_ioService), DefaultBufferSize))
    , m_endpoint(new ip::tcp::endpoint(ip::address::from_string(address), port))
    , m_timer(new std::remove_pointer_t<decltype (m_timer)>(*m_ioService))
    , m_handlerPakageRecived(nullptr)
    , m_handlerSessionStatusChanged(nullptr)
    , m_pingErrorCounter(0)
{
    m_session->bindReadErrorRecived(std::bind(&ClientPrivate::readError, this, std::placeholders::_1));
    m_session->bindWriteErrorRecived(std::bind(&ClientPrivate::writeError, this, std::placeholders::_1));
    m_session->bindPackageRecived([=](const Package & pack)
    {
        if (pack.data().size() == ::ServerReply.size())
        {
            if(std::strcmp(pack.data().data(), ::ServerReply.data()) == 0)
            {
                m_pingErrorCounter = 0;
                return;
            }
        }
        if (m_handlerPakageRecived != nullptr)
        {
            m_handlerPakageRecived(pack);
        }
    });

    connect();
    std::thread *tr = new std::thread(std::bind(&ClientPrivate::run, this));
    tr->detach();
}

ClientPrivate::~ClientPrivate()
{
    delete m_endpoint;
    delete m_timer;
    m_session->getSocket().close();
    delete m_session;
    delete m_ioService;
}

void ClientPrivate::readError(std::string message)
{
    std::cout << "read error:" << message;
    m_timer->cancel();
    m_session->getSocket().close();

    if (m_handlerErrorRecived != nullptr)
    {
        m_handlerErrorRecived(message);
    }
}

void ClientPrivate::writeError(std::string message)
{
    std::cout << "write error:" << message;
    if (m_handlerErrorRecived != nullptr)
    {
        m_handlerErrorRecived(message);
    }
}

std::string ClientPrivate::ip()
{
    return m_session->ip();
}

ClientIface::Status ClientPrivate::sessionStatus()
{
    return m_sessionStatus;
}

void ClientPrivate::bindErrorRecived(std::function<void (std::string)> handler)
{
    m_handlerErrorRecived = handler;
}

void ClientPrivate::bindPackageRecived(std::function<void (const Package &)> handler)
{
    m_handlerPakageRecived = handler;
}

void ClientPrivate::bindSessionStatusChanged(std::function<void (ClientIface::Status)> handler)
{
    m_handlerSessionStatusChanged = handler;
}

bool ClientPrivate::sendPackage(const Package &package)
{
    m_session->sendPackage(package);
}

void ClientPrivate::connect()
{
    debug() << "Connect";
    ip::tcp::resolver::iterator it;
    m_session->getSocket().async_connect(*m_endpoint,
                                         [=](boost::system::error_code er_c)
    {
        if (!er_c) // true if error
        {
            debug() << "Connected" << er_c.message();
            doPing();
            m_session->doRead();
            return;
        }
        debug() << "Error sesson:" << er_c.message();
    });
}

void ClientPrivate::run()
{
    debug() << "Start Session";
    m_ioService->run();
    debug() << "Stop Session";
}

void ClientPrivate::handleSessionStatusChanged(const ClientIface::Status &state)
{
    if (m_sessionStatus != state)
    {
        m_sessionStatus = state;
        if (m_handlerSessionStatusChanged == nullptr)
        {
            debug() << "m_handlerSessionStatusChanged is null";
            return;
        }
        m_handlerSessionStatusChanged(state);
    }
}

void ClientPrivate::doPing()
{
    debug() << "";
    m_timer->expires_from_now(boost::posix_time::seconds(5));
    m_timer->async_wait(std::bind(&ClientPrivate::handlePing, this));
}

void ClientPrivate::handlePing()
{
    debug() << "";
    static Package pingPack(ClientPing);
    m_session->sendPackage(pingPack);
    m_pingErrorCounter+= 1;
    handleSessionStatusChanged(m_session->getSocket().is_open()
                               ? m_pingErrorCounter > PingErrorThreshold
                                 ? ClientIface::NotRespond : ClientIface::Connected
                               : ClientIface::Disconnected);
    if (m_sessionStatus != ClientIface::Disconnected)
    {
        doPing();
    }
}
