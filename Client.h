/*
** Copyright (c) 2020 Domnin V.N.
** All rights reserved
**
**
*/

#ifndef CLIENT_PRIVATE_H
#define CLIENT_PRIVATE_H

#include <string>
#include <SessionIface.h>
#include <ClientIface.h>

class Package;

class SessionIface;

class ClientPrivate : public ClientIface
{
public:
    ClientPrivate(std::string &address, ushort port);
    ~ClientPrivate() override;

    void connect() override;
    std::string ip() override;
    bool sendPackage(const Package &package) override;
    ClientIface::Status sessionStatus() override;

    void bindErrorRecived(std::function<void (std::string)> handler) override;
    void bindPackageRecived(std::function<void (const Package &)> handler) override;
    void bindSessionStatusChanged(std::function<void (ClientIface::Status)> handler) override;

    bool sendData(std::vector<char> data);

private:
    void handleSessionStatusChanged(const Status &state);
    void run();
    void doPing();
    void handlePing();

    void readError(std::string message);
    void writeError(std::string message);

    ClientIface::Status m_sessionStatus;

    boost::asio::io_service *const m_ioService;
    SessionIface *m_session;
    boost::asio::ip::tcp::endpoint *m_endpoint;
    boost::asio::deadline_timer *const m_timer;

    //Callbacks
    std::function<void (std::string)> m_handlerErrorRecived;
    std::function<void (const Package &)> m_handlerPakageRecived;
    std::function <void (Status)> m_handlerSessionStatusChanged;

    uint m_pingErrorCounter;
};

#endif // CLIENT_PRIVATE_H
